#include "projectwindow.h"

#include <QFileDialog>
#include <QMenuBar>

ProjectWindow::ProjectWindow(QWidget *parent) :
    QMainWindow(parent)
{

    createActions();
    createMenu();

}

ProjectWindow::~ProjectWindow()
{
    delete pProjectDirectory;
}

void ProjectWindow::openProject()
{
    QString directory = QFileDialog::getExistingDirectory(this, tr("Directorio del proyecto"), ".");
    if(!directory.isEmpty())
    {
        checkDirectory(directory);
    }
}

bool ProjectWindow::checkDirectory(const QString &filename)
{
    QDir *dir = new QDir(filename);
    if(!dir->exists())
        if(!dir->mkpath("."))
            return false;

    // Check drawable-mdpi
    if(!dir->mkdir("drawable-mdpi"))
        return false;
    // Check drawable-hdpi
    if(!dir->mkdir("drawable-hdpi"))
        return false;
    // Check drawable-xhdpi
    if(!dir->mkdir("drawable-xhdpi"))
        return false;
    // Check drawable-xxhdpi
    if(!dir->mkdir("drawable-xxhdpi"))
        return false;
    // Check drawable-xxxhdpi
    if(!dir->mkdir("drawable-xxxhdpi"))
        return false;

    pProjectDirectory = new QDir(dir);
    return true;

}

// Busca todas las imagenes recursivamente
int ProjectWindow::searchImages()
{
    QDir dir(pProjectDirectory->absoluteFilePath() + QDir::separator() + "mdpi");
    qlonglong size = 0;

    QStringList filters;
    foreach (QByteArray format, QImageReader::supportedImageFormats())
        filters += "*." + format;

    foreach (QString file, dir.entryList(filters, QDir::Files))
    {
        imageFound(QFileInfo(dir, file).absoluteFilePath());
        size++;
    }
    return size;
}

// CArga todas las imagenes recibidas en el ListWidget
void ProjectWindow::imageFound(const QString &image)
{

}

void ProjectWindow::createActions()
{
    pOpenProjectAction = new QAction(tr("&Open Project"), this);
    pOpenProjectAction->setToolTip(tr("Selecciona un directorio q contenga una estructura drawable"));
    connect(pOpenProjectAction, SIGNAL(triggered()), this, SLOT(openProject()));
}

void ProjectWindow::createMenu()
{
    pFileMenu = menuBar()->addMenu(tr("&File"));
    pFileMenu->addAction(pOpenProjectAction);
}
