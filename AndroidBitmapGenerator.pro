#-------------------------------------------------
#
# Project created by QtCreator 2014-06-25T13:02:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AndroidBitmapGenerator
TEMPLATE = app


SOURCES += main.cpp\
        projectwindow.cpp

HEADERS  += projectwindow.h

FORMS    +=
