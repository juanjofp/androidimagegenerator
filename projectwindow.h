#ifndef PROJECTWINDOW_H
#define PROJECTWINDOW_H

#include <QListWidget>
#include <QDir>
#include <QAction>
#include <QMenu>

#include <QMainWindow>

class ProjectWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ProjectWindow(QWidget *parent = 0);
    ~ProjectWindow();

signals:
    void imageFound(const QString &image);

private slots:
    void openProject();

private:
    QListWidget *pImagesList;
    QDir *pProjectDirectory;
    int searchImages();

    // Create actions
    void checkDirectory(const QString &filename);
    void createActions();
    void createMenu();

    // Actions
    QAction *pOpenProjectAction;

    // Menu
    QMenu *pFileMenu;
};

#endif // PROJECTWINDOW_H
