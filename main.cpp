#include "projectwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ProjectWindow w;
    w.show();

    return a.exec();
}
